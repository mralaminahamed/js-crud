# Simple JS Crud Application
I have created a Simple JS CRUD application with vanilla js and css.

## What can i do?
I can do with this application, add new data to table, edit any data and delete from this table.

## Screenshots
<img src="https://raw.githubusercontent.com/mralaminahamed/js-crud/main/screenhots/Empty-page-JS-Crud.png"/>
<img src="https://raw.githubusercontent.com/mralaminahamed/js-crud/main/screenhots/data -table-JSCrud.png"/>
<img src="https://raw.githubusercontent.com/mralaminahamed/js-crud/main/screenhots/edit-JS-Crud.png"/>

## Live Demo
demo site https://peaceful-noyce-2711a0.netlify.app/
